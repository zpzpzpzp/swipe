/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.pwittchen.swip.app;

import com.github.pwittchen.swip.library.rx2.Swipe;
import com.github.pwittchen.swip.library.rx2.SwipeListener;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 主程序入口
 */
public class MainAbility extends Ability {
    private Text info;
    private Swipe swipe;
    private DirectionalLayout dirView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        info = (Text) findComponentById(ResourceTable.Id_info);
        dirView = (DirectionalLayout) findComponentById(ResourceTable.Id_dir);
        Text textTitle = (Text) findComponentById(ResourceTable.Id_title1);
        textTitle.setText("滑动监听普通实现");
        Text textButton = (Text) findComponentById(ResourceTable.Id_rxbutton);
        textButton.setText("Rx实现");
        textButton.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        Intent intent1 = new Intent();
                        Operation operation =
                                new Intent.OperationBuilder()
                                        .withDeviceId("")
                                        .withBundleName("com.github.pwittchen.swip.app")
                                        .withAbilityName("com.github.pwittchen.swip.app.SwipRxAbility")
                                        .build();
                        intent1.setOperation(operation);
                        startAbility(intent1);
                    }
                });
        swipListener();
        dirView.setTouchEventListener(
                new Component.TouchEventListener() {
                    @Override
                    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                        swipe.dispatchTouchEvent(touchEvent);
                        return true;
                    }
                });

    }

    private void swipListener() {
        swipe = new Swipe();
        swipe.setListener(
                new SwipeListener() {
                    @Override
                    public void onSwipingLeft(final TouchEvent event) {
                        info.setText("SWIPING_LEFT");
                    }

                    @Override
                    public boolean onSwipedLeft(final TouchEvent event) {
                        info.setText("SWIPED_LEFT");
                        return false;
                    }

                    @Override
                    public void onSwipingRight(final TouchEvent event) {
                        info.setText("SWIPING_RIGHT");
                    }

                    @Override
                    public boolean onSwipedRight(final TouchEvent event) {
                        info.setText("SWIPED_RIGHT");
                        return false;
                    }
                    @Override
                    public void onSwipingDown(final TouchEvent event) {
                        info.setText("SWIPING_DOWN");
                    }

                    @Override
                    public boolean onSwipedDown(final TouchEvent event) {
                        info.setText("SWIPED_DOWN");
                        return false;
                    }
                    @Override
                    public void onSwipingUp(final TouchEvent event) {
                        info.setText("SWIPING_UP");
                    }

                    @Override
                    public boolean onSwipedUp(final TouchEvent event) {
                        info.setText("SWIPED_UP");
                        return false;
                    }


                });
    }
}
